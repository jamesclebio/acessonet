<section class="background color-lightest cover" style="background-image: url(<?php echo $this->_asset('images/background/macbook-corner.jpg'); ?>);">
  <div class="default-container">
    <div class="grid x-end y-center height-400">
      <div class="item size-4 text center">
        <h4 class="heading color muted text hiper">Ainda com dúvidas?</h4>
        <a href="<?php echo $this->_url('atendimento'); ?>" class="link more text large thin">Tente o nosso atendimento</a>
      </div>
    </div>
  </div>
</section>
