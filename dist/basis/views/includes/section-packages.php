<section class="default-container">
  <div class="grid x-center y-center">
    <div class="item size-8 text center">
      <h4 class="heading">Quer Acessonet na sua casa?</h4>

      <div class="reading">
        <p class="">Escolha o seu plano ideal e solicite assinatura. <strong>É simples, prático e fácil.</strong></p>
        <p><a href="<?php echo $this->_url('planos'); ?>" class="button large warning">Assine já</a></p>
      </div>
    </div>
  </div>
</section>
