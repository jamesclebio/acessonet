<section class="slider">
  <div class="item theme-4" style="background-image: url('<?php echo $this->_asset('images/background/business-glasses.jpg'); ?>');">
    <div id="particles-js-1"></div>
    <div class="default-container">
      <div class="block text">
        <div class="primary">Tecnologia pensada para a performance de seu negócio</div>
        <div class="secondary">Utilize os serviços Acessonet e se preocupe apenas com os resultados</div>
        <a href="<?php echo $this->_url('servicos'); ?>" class="button large warning">Conhecer serviços</a>
      </div>
    </div>
  </div>
</section>

<section class="background">
  <div class="default-container">
    <div class="grid x-end y-center">
      <div class="item">
        <div class="thumbnail size-400 transparent animation floating">
          <img src="<?php echo $this->_asset('images/imac-positive.png'); ?>" alt="">
        </div>
      </div>

      <div class="item">
        <h4 class="heading">A Acessonet é sempre a melhor opção</h4>

        <ul class="list-icon">
          <li><i class="fa fa-check"></i>Os serviços da Acessonet são projetados para terem o melhor custo/benefício e garantia de qualidade.</li>
          <li><i class="fa fa-check"></i>O respeito com os clientes norteia o trabalho da Acessonet, que tem o compromisso de prestar os seus serviços com competência e qualidade.</li>
        </ul>

        <a href="<?php echo $this->_url('servicos'); ?>" class="button large expanded bordered warning gap top">Vamos conversar?</a>
      </div>
    </div>
  </div>
</section>
