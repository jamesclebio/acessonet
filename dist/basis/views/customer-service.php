<section class="height-300 background color-lighter">
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3920.9893951892104!2d-37.3083762854727!3d-10.657930592402039!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x70ff62d3e923213%3A0x7fe89d2e67d5e466!2sR.+De+Itabaiana%2C+23%2C+Malhador+-+SE%2C+49570-000!5e0!3m2!1sen!2sbr!4v1499263662190" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>

<section class="default-container">
  <ul class="breadcrumb">
    <li><a href="<?php echo $this->_url('root'); ?>">Home</a></li>
    <li class="on"><a href="<?php echo $this->_url('atendimento'); ?>">Atendimento</a></li>
  </ul>

  <div class="grid x-justify">
    <div class="item size-5">
      <h2 class="heading color highlighted-custom-2 text hiper">Quer falar com a Acessonet?</h2>

      <nav class="tab expanded">
        <ul>
          <li><a href="#tabChannels" data-tab class="on">Use nossos canais</a></li>
          <li><a href="#tabMessage" data-tab="(<?php echo $this->_url('customer-service/message'); ?>)">Envie sua mensagem</a></li>
        </ul>
      </nav>

      <div class="gap top" data-tab="content">
        <div id="tabChannels">
          <div class="reading">
            <p>Fique à vontade para utilizar os nossos canais de atendimento e esclarecer dúvidas ou solicitar outras informações.</p>
            <p><strong>Nós queremos lhe ouvir e teremos prazer em lhe atender!</strong></p>
          </div>
        </div>

        <div id="tabMessage"></div>
      </div>
    </div>

    <div class="item size-6">
      <div class="card">
        <div class="container">
          <div class="header">Endereço</div>

          <div class="body">
            <ul class="list-icon">
              <li>
                <i class="fa fa-map-marker"></i>
                <address class="nomargin">
                  <strong>Malhador, SE</strong><br>
                  Rua Itabaiana, 23, Centro<br>
                  CEP 49570-000
                </address>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div class="card">
        <div class="container">
          <div class="header">Telefones</div>
          <div class="body">
            <ul class="list-icon">
              <li><i class="fa fa-phone"></i><a href="tel:07932117052"><strong>(79) 3211-7052</strong></a></li>
              <li><i class="fa fa-phone"></i><a href="tel:07988649946"><strong>(79) 8864-9946</strong></a><small class="color mute"> - Oi</small></li>
              <li><i class="fa fa-phone"></i><a href="tel:07996034425"><strong>(79) 9603-4425</strong></a><small class="color mute"> - Vivo</small></li>
              <li><i class="fa fa-phone"></i><a href="tel:07981702555"><strong>(79) 8170-2555</strong></a><small class="color mute"> - Claro</small></li>
            </ul>
          </div>
        </div>
      </div>

      <div class="card">
        <div class="container">
          <div class="header">Email</div>
          <div class="body">
            <ul class="list-icon">
              <li><i class="fa fa-envelope"></i><a href="mailto:atendimento@acessonet.com.br">atendimento@acessonet.com.br</a></li>
            </ul>
          </div>
        </div>
      </div>

      <div class="card">
        <div class="container">
          <div class="header">Redes sociais</div>
          <div class="body">
            <ul class="list-icon">
              <li title="Instagram"><i class="fa fa-instagram"></i><a href="#" target="_blank">acessonet</a></li>
              <li title="Twitter"><i class="fa fa-twitter"></i><a href="#" target="_blank">acessonet</a></li>
              <li title="Facebook"><i class="fa fa-facebook-official"></i><a href="#" target="_blank">acessonetprovedor</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
