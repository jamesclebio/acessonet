<section class="default-container">
  <ul class="breadcrumb">
    <li><a href="<?php echo $this->_url('root'); ?>">Home</a></li>
    <li class="on"><a href="<?php echo $this->_url('privacidade'); ?>">Política de privacidade</a></li>
  </ul>

  <div class="grid x-center">
    <div class="item size-8">
      <h1 class="heading text center">Política de privacidade</h1>

      <div class="reading">
        <h5>Comprometimento da Acessonet</h5>
        <ul>
          <li>Coletar em seu website apenas as informações sobre identificação individual necessárias à viabilização de seus negócios e ao fornecimento de produtos e/ou serviços solicitados por seus clientes.</li>
          <li>Utilizar cookies apenas para controlar a audiência e a navegação em seu website.</li>
          <li>Cumprir rigorosamente todas as determinações desta Política de Privacidade e Segurança de Dados.</li>
        </ul>

        <h5>Utilização das informações</h5>
        <p>As informações capturadas pelos domínios e sub-domínios da <strong>acessonet.com.br</strong> são utilizadas pela Acessonet com a finalidade de:</p>
        <ul>
          <li>Viabilizar o fornecimento de produtos ou serviços solicitados no website.</li>
          <li>Identificar o perfil, desejos ou necessidades dos usuários, a fim de aprimorar os produtos e/ou serviços oferecidos pela empresa.</li>
          <li>Enviar informativos sobre produtos ou serviços que interessem aos seus usuários.</li>
          <li>Divulgar alterações, inovações ou promoções sobre os produtos e serviços da empresa Acessonet e de seus parceiros.</li>
        </ul>

        <h5>Divulgação de dados</h5>
    	  <p>A Acessonet não fornece a terceiros dados sobre a identificação individual de usuários, sem seu prévio consentimento, exceto nos casos em que:</p>
        <ul>
    	    <li>Haja determinação judicial para fornecimento de dados.</li>
    	    <li>A viabilização dos negócios e/ou serviços oferecidos pela Acessonet dependa do repasse de dados a parceiros.</li>
    	    <li>Exista a necessidade de identificar ou revelar dados do usuário que esteja utilizando o seu website com propósitos ilícitos (intencionalmente ou não).</li>
    	    <li>Além dos casos acima citados, havendo a necessidade ou interesse em repassar a terceiros dados de identificação individual dos usuários, a Acessonet lhes solicitará autorização prévia.</li>
    	    <li>Terceiros que, por ventura, receberem da Acessonet informações, de qualquer natureza, sobre os internautas que acessam o seu website cabe, igualmente, a responsabilidade de zelar pelo sigilo e segurança das referidas informações.</li>
        </ul>

        <h5>Remoção de contatos</h5>
        <ul>
          <li>A Acessonet se compromete a respeitar a sua privacidade e é contra o spam. Caso não deseje mais receber informações, envie seu nome completo, email e CPF para <a href="mailto:privacidade@acessonet.com.br">privacidade@acessonet.com.br</a>.</li>
          <li>Em caso de dúvidas envie mensagem para <a href="mailto:privacidade@acessonet.com.br">privacidade@acessonet.com.br</a>.</li>
        </ul>
      </div>
    </div>
  </div>
</section>
