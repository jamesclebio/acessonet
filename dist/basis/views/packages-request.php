<!-- Modal -->
<section class="content">
  <?php if ($this->form): ?>
    <header class="header">
      <h3>Solicitar plano <?php echo $this->packageFullName; ?></h3>
      <h4>Preencha o formulário para continuar com a sua solicitação</h4>
    </header>
  <?php endif; ?>

  <div class="body">
    <?php if ($this->success): ?>
      <div class="board success">
        <div class="grid y-center">
          <div class="item size-4">
            <div class="text center ultra">
              <i class="fa fa-check"></i>
            </div>
          </div>

          <div class="item size-8">
            <div class="reading">
              <p class="text large"><strong>Tudo certo, <?php echo $this->firstName; ?>!</strong></p>
              <p>A sua solicitação de assinatura para o plano <strong><?php echo $this->packageFullName; ?></strong> foi enviada com sucesso.</p>
              <p>O nosso atendimento entrará em contato o mais breve possível para concluir a sua assinatura.</p>
              <p><strong>Obrigado por escolher a Acessonet!</strong></p>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($this->form): ?>
      <form action="<?php echo $this->_url('packages/request/id/' . $this->package['id']); ?>" method="POST" class="form" data-form-toggle>
        <input name="redbutton" type="text" tabindex="-1" autocomplete="off">

        <fieldset>
          <legend>Dados pessoais</legend>

          <div class="input">
            <label>
              Nome completo *
              <input name="name" type="text" required autofocus>
            </label>
          </div>

          <div class="grid">
            <div class="item">
              <div class="input">
                <label>
                  CPF *
                  <input name="cpf" type="text" required data-mask="cpf(autoblur)">
                </label>
              </div>
            </div>

            <div class="item">
              <div class="input">
                <label>
                  RG *
                  <input name="rg" type="text" required>
                </label>
              </div>
            </div>
          </div>

          <div class="grid">
            <div class="item">
              <div class="input">
                <label>
                  Email *
                  <input name="email" type="email" required>
                </label>
              </div>
            </div>

            <div class="item">
              <div class="input">
                <label>
                  Telefone *
                  <input name="phone" type="tel" required data-mask="phone(autoblur)">
                </label>
              </div>
            </div>
          </div>
        </fieldset>

        <fieldset>
          <legend>Dados para instalação</legend>

          <div class="grid">
            <div class="item">
              <div class="input">
                  <label>
                    Localidade *
                    <input name="city" type="text" required readonly value="<?php echo AuthenticationHelper::getAreaFullName(); ?>">
                  </label>
              </div>
            </div>

            <div class="item">
              <div class="input">
                  <label>
                    CEP *
                    <input name="zipcode" type="text" required data-mask="zipcode(autoblur)">
                  </label>
              </div>
            </div>
          </div>

          <div class="input">
            <label>
              Endereço *
              <input name="address" type="text" required>
            </label>
          </div>

          <div class="grid">
            <div class="item">
              <div class="input">
                <label>
                  Número
                  <input name="number" type="text">
                </label>
              </div>
            </div>

            <div class="item">
              <div class="input">
                <label>
                  Complemento
                  <input name="supplement" type="text">
                </label>
              </div>
            </div>

            <div class="item">
              <div class="input">
                <label>
                  Bairro
                  <input name="district" type="text">
                </label>
              </div>
            </div>
          </div>
        </fieldset>

        <div class="control-bar">
          <button class="button custom-2">Confirmar</button>
        </div>
      </form>
    <?php endif; ?>
  </div>
</section>

<script>
  main.init('formToggle', 'formMask');
</script>
