<!-- Modal -->
<section class="content">
  <header class="header">
    <h3>Escolher localidade</h3>
    <h4>Selecione uma localidade para que possamos lhe oferecer as melhores ofertas</h4>
  </header>

  <div class="body">
    <ul class="list-area">
      <?php foreach ($this->getAreas() as $data): ?>
        <li><a href="<?php echo $this->_url('session/set/area/' . $data['id'] . ''); ?>"><?php echo $data['name'] . ', ' . $data['state']; ?></a></li>
      <?php endforeach; ?>
    </ul>
  </div>
</section>
