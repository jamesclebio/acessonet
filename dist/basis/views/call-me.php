<?php if ($this->success): ?>
  <div class="board success text center">
    <div class="heading text mega">
      <i class="fa fa-check"></i>
    </div>

    <div class="reading">
      <p class="text large"><strong>Tudo certo, <?php echo $this->firstName; ?>!</strong></p>
      <p>Agora é só aguardar o contato de nosso atendimento, que será o mais breve possível.</p>
      <p><strong>Obrigado por escolher a Acessonet!</strong></p>
    </div>
  </div>
<?php endif; ?>

<?php if ($this->form): ?>
  <div class="reading">
    <p>Informe alguns dados para que o nosso atendimento entre em contato com você:</p>
  </div>

  <form action="<?php echo $this->_url('call-me/request'); ?>" method="POST" class="form gap top" data-form-toggle>
    <input name="redbutton" type="text" tabindex="-1" autocomplete="off">

    <fieldset>
      <div class="input">
        <label>
          CEP *
          <input name="zipcode" type="text" required autofocus data-mask="zipcode(autoblur)">
        </label>

        <small>Para verificação de disponiblidade de serviço</small>
      </div>

      <div class="input">
        <label>
          Seu nome *
          <input name="name" type="text" required>
        </label>
      </div>

      <div class="input">
        <label>
          Seu telefone *
          <input name="phone" type="tel" required data-mask="phone(autoblur)">
        </label>
      </div>
    </fieldset>

    <div class="control-bar">
      <button class="button custom-2">Me ligue</button>
    </div>
  </form>

  <script>
    main.init('formToggle', 'formMask');
  </script>
<?php endif; ?>
