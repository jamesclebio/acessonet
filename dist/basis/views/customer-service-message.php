<?php if ($this->success): ?>
  <div class="board success text center">
    <div class="heading text mega">
      <i class="fa fa-check"></i>
    </div>

    <div class="reading">
      <p class="text large"><strong>Tudo certo, <?php echo $this->firstName; ?>!</strong></p>
      <p>A nossa equipe recebeu a sua mensagem e dará um retorno o mais breve possível.</p>
      <p><strong>Obrigado por escolher a Acessonet!</strong></p>
    </div>
  </div>
<?php endif; ?>

<?php if ($this->form): ?>
  <form action="<?php echo $this->_url('customer-service/message'); ?>" method="POST" class="form" data-form-toggle>
    <input name="redbutton" type="text" tabindex="-1" autocomplete="off">

    <fieldset>
      <div class="input">
        <label>
          Seu nome *
          <input name="name" type="text" required autofocus>
        </label>
      </div>

      <div class="input">
        <label>
          Seu email *
          <input name="email" type="email" required>
        </label>
      </div>

      <div class="input">
        <label>
          Seu telefone *
          <input name="phone" type="tel" required data-mask="phone(autoblur)">
        </label>
      </div>

      <div class="input">
        <label>
          Mensagem *
          <textarea name="message" cols="30" rows="10"></textarea>
        </label>
      </div>
    </fieldset>

    <div class="control-bar">
      <button class="button custom-2">Enviar</button>
    </div>
  </form>

  <script>
    main.init('formToggle', 'formMask');
  </script>
<?php endif; ?>
