<section class="default-container">
  <ul class="breadcrumb">
    <li><a href="<?php echo $this->_url('session/set/profile/residential'); ?>">Você</a></li>
    <li class="on"><a href="<?php echo $this->_url('planos'); ?>">Planos</a></li>
  </ul>

  <div class="grid x-justify">
    <div class="item size-5">
      <h2 class="heading color highlighted-custom-2 text hiper">Quer Acessonet na sua casa?</h2>

      <nav class="tab expanded">
        <ul>
          <li><a href="#tabOnline" data-tab class="on">Solicite online</a></li>
          <li><a href="#tabCallMe" data-tab="(<?php echo $this->_url('call-me'); ?>)">Nós te ligamos</a></li>
        </ul>
      </nav>

      <div class="gap top" data-tab="content">
        <div id="tabOnline">
          <div class="reading">
            <p>Para iniciar a sua assinatura online basta escolher o plano que melhor se encaixa ao seu dia a dia e clicar no botão <strong>solicitar</strong>.</p>
            <p><strong>É simples, prático e fácil.</strong></p>
          </div>
        </div>

        <div id="tabCallMe"></div>
      </div>
    </div>

    <div class="item size-6">
      <?php if ($this->getPackages()): ?>
        <?php foreach ($this->getPackages() as $data): ?>
          <div class="package">
            <div class="container">
              <div class="name">Plano <?php echo $data['name']; ?></div>
              <div class="tag"><?php echo $data['tag']; ?></div>

              <div class="details">
                <?php if ($data['wifi']): ?>
                  <div class="wifi">WiFi grátis</div>
                <?php endif; ?>
                <div class="download">Download até <?php echo $data['download']; ?></div>
                <div class="upload">Upload até <?php echo $data['upload']; ?></div>
                <?php if ($data['tiny']): ?>
                  <div class="tiny"><?php echo $data['tiny']; ?></div>
                <?php endif; ?>
              </div>

              <div class="price">
                R$ <?php echo intval($data['price']); ?><small>,<?php echo explode('.', $data['price'])[1]; ?><br>p/mês</small>
                <button type="button" data-modal="(<?php echo $this->_url('packages/request/id/' . $data['id']); ?>)">Solicitar</button>
              </div>
            </div>
          </div>
        <?php endforeach; ?>

        <div class="board gap top">
          <div class="reading">
            <ul class="color muted text small">
              <li>Planos válidos para <strong><?php echo AuthenticationHelper::getAreaFullName(); ?></strong>.</li>
              <li>Todo material e/ou equipamento, quando fornecido pela Acessonet, é em comodato. Após a vigência contratual ou mediante cancelamento poderá ser recolhido, exclusivamente, pela Acessonet. Sujeito a disponibilidade em estoque.</li>
              <li>Instalação sujeita a condições técnicas. Consulte disponibilidade.</li>
            </ul>
          </div>
        </div>

      <?php else: ?>
        <div class="board text center color muted">
          <div class="reading">
            <p><strong>Oops! Nenhum plano para <?php echo AuthenticationHelper::getAreaFullName(); ?>...</strong></p>
            <p class="text small">Tente uma <a href="<?php echo $this->_url('area'); ?>" data-modal>outra localidade</a> ou fale com o <a href="<?php echo $this->_url('atendimento'); ?>">nosso atendimento</a> para consultar disponibilidade na região.</p>
          </div>
        </div>
      <?php endif; ?>
    </div>
  </div>
</section>
