<section class="default-container">
  <ul class="breadcrumb">
    <li><a href="<?php echo $this->_url('root'); ?>">Home</a></li>
    <li class="on"><a href="<?php echo $this->_url('jobs'); ?>">Trabalhe conosco</a></li>
  </ul>

  <div class="grid x-justify">
    <div class="item size-5">
      <h2 class="heading color highlighted-custom-1 text hiper">Faça parte da família Acessonet</h2>

      <div class="reading">
        <p>Buscamos colaboradores que:</p>
      </div>

      <ul class="list-icon gap top">
        <li><i class="fa fa-check"></i>Estejam preparados e motivados para assumir riscos, desafios e atitudes inovadoras</li>
        <li><i class="fa fa-check"></i>Vistam a camisa da empresa</li>
        <li><i class="fa fa-check"></i>Sejam proativos</li>
        <li><i class="fa fa-check"></i>Saibam ouvir</li>
        <li><i class="fa fa-check"></i>Respeitem as relações internas</li>
        <li><i class="fa fa-check"></i>Tenham muita vontade de crescer</li>
      </ul>
    </div>

    <div class="item size-6">
      <div class="board">
        <form action="<?php echo $this->_url('jobs/apply'); ?>" method="POST" class="form" data-form-toggle>
          <input name="redbutton" type="text" tabindex="-1" autocomplete="off">

          <fieldset>
            <legend>Cadastro de currículo</legend>

            <div class="input">
              <label>
                Nome *
                <input name="name" type="text" required>
              </label>
            </div>

            <div class="input">
              <label>
                Email *
                <input name="email" type="email" required>
              </label>
            </div>

            <div class="input">
              <label>
                Telefone *
                <input name="phone" type="tel" required data-mask="phone(autoblur)">
              </label>
            </div>

            <div class="input">
              <label>Área de interesse *</label>
              <label class="check inline text"><input name="target" type="radio" value="Administrativo" required> Administrativa</label>
              <label class="check inline text"><input name="target" type="radio" value="Vendas"> Vendas</label>
              <label class="check inline text"><input name="target" type="radio" value=""> Técnica</label>
            </div>

            <div class="input">
              <label>
                Currículo *
                <input name="curriculum" type="file" required>
                <small>Selecione um arquivo <strong>PDF</strong>, com até <strong>2MB</strong>.</small>
              </label>
            </div>

            <div class="input">
              <label>
                Informações adicionais
                <textarea name="additional" cols="30" rows="10"></textarea>
              </label>
            </div>
          </fieldset>

          <div class="control-bar">
            <button class="button custom-2">Enviar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
