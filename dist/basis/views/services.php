<section class="default-container">
  <ul class="breadcrumb">
    <li><a href="<?php echo $this->_url('session/set/profile/business'); ?>">Empresa</a></li>
    <li class="on"><a href="<?php echo $this->_url('servicos'); ?>">Serviços</a></li>
  </ul>

  <div class="grid x-justify">
    <div class="item size-5">
      <h2 class="heading color highlighted-custom-1 text hiper">Quer Acessonet em seu negócio?</h2>

      <div class="position relative">
        <div class="reading">
          <p>Informe alguns dados para que um de nossos especialistas entre em contato com você:</p>
        </div>

        <form action="<?php echo $this->_url('services/request'); ?>" method="POST" class="form gap top" data-form-toggle>
          <input name="redbutton" type="text" tabindex="-1" autocomplete="off">

          <fieldset>
            <div class="input">
              <label>
                Empresa *
                <input name="business" type="text" required>
              </label>
            </div>

            <div class="input">
              <label>
                Seu nome *
                <input name="name" type="text" required>
              </label>
            </div>

            <div class="input">
              <label>
                Seu email *
                <input name="email" type="email" required>
              </label>
            </div>

            <div class="input">
              <label>
                Seu telefone *
                <input name="phone" type="tel" required data-mask="phone(autoblur)">
              </label>
            </div>

            <div class="input">
              <label>
                Mensagem *
                <textarea name="message" cols="30" rows="10"></textarea>
              </label>
            </div>
          </fieldset>

          <div class="control-bar">
            <button class="button custom-2">Enviar</button>
          </div>
        </form>
      </div>
    </div>

    <div class="item size-6">
      <div class="card">
        <div class="container">
          <div class="header">Internet Corporativa</div>

          <div class="body">
            <p>Sinônimo de confiabilidade, links dedicados de internet garantem mais estabilidade em sua conexão. Nesta modalidade a banda de dados (download e upload) é reservada e simétrica, ideal para sua empresa estar sempre conectada, com disponibilidade total da banda contratada.</p>
          </div>

          <div class="image animation floating">
            <img src="<?php echo $this->_asset('images/icon-business/011-settings-4.svg'); ?>" alt="">
          </div>
        </div>
      </div>

      <div class="card">
        <div class="container">
          <div class="header">Internet ISP</div>

          <div class="body">
            <p>Serviço de internet dedicada para provedores de acesso que necessitam de altas velocidades e 100% disponível, possibilitando realizar anúncios do seu ASN e de seus parceiros.</p>
          </div>

          <div class="image animation floating">
            <img src="<?php echo $this->_asset('images/icon-business/064-startup.svg'); ?>" alt="">
          </div>
        </div>
      </div>

      <div class="card">
        <div class="container">
          <div class="header">Rede virtual MPLS</div>

          <div class="body">
            <p>De forma eficiente e segura é possível transportar dados entre redes locais (lan-to-lan) de unidades da sua empresa, oferecendo total privacidade à rede IP e aos dados trafegados e, o melhor, com baixo custo.</p>
          </div>

          <div class="image animation floating">
            <img src="<?php echo $this->_asset('images/icon-business/063-padlock.svg'); ?>" alt="">
          </div>
        </div>
      </div>

      <div class="card">
        <div class="container">
          <div class="header">P2P ou PTP (Peer-to-Peer)</div>

          <div class="body">
            <p>A conexão ponto a ponto é ideal para empresas que possuem filiais e desejam se comunicar de forma segura e escalável. Este serviço possibilita o compartilhamento de todos os recursos de uma rede local, como dados e voz, podendo gerar uma economia significativa.</p>
          </div>

          <div class="image animation floating">
            <img src="<?php echo $this->_asset('images/icon-business/076-magnet.svg'); ?>" alt="">
          </div>
        </div>
      </div>

      <div class="card">
        <div class="container">
          <div class="header">Projetos de redes</div>

          <div class="body">
            <p>Um projeto bem elaborado e executado é fundamental para o bom funcionamento da rede de dados e voz da sua empresa. Podemos realizar todo o cabeamento estruturado, óptico, wireless e configuração da sua rede.</p>
          </div>

          <div class="image animation floating">
            <img src="<?php echo $this->_asset('images/icon-business/089-flask.svg'); ?>" alt="">
          </div>
        </div>
      </div>

      <div class="card">
        <div class="container">
          <div class="header">Condominis</div>

          <div class="body">
            <p>Seu condomínio conectado de forma inteligente. Imagine receber fibra ótica em sua residência e usufruir de serviços de rede local e internet de alta velocidade e disponibilidade. Além disso, ter todo o sistema de comunicação e segurança acessível de qualquer lugar.</p>
          </div>

          <div class="image animation floating">
            <img src="<?php echo $this->_asset('images/icon-business/034-tablet-1.svg'); ?>" alt="">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
