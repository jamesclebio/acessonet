<section class="default-container">
  <ul class="breadcrumb">
    <li><a href="<?php echo $this->_url('root'); ?>">Home</a></li>
    <li><a href="<?php echo $this->_url('ajuda'); ?>">Central de ajuda</a></li>
    <?php if ($this->helpPost): ?>
      <li class="on"><?php echo $this->helpPost['categorie']; ?></li>
    <?php endif; ?>
  </ul>

  <div class="grid x-center">
    <div class="item size-8">
      <?php if ($this->helpPost): ?>
        <h1 class="heading text center"><?php echo $this->helpPost['title']; ?></h1>

        <div class="heading color muted text small">Publicado em <?php echo date('d/m/Y, H:i:s (\G\M\T P)', strtotime($this->helpPost['published'])); ?></div>

        <div class="reading">
          <?php echo $this->helpPost['body']; ?>
        </div>

      <?php else: ?>
        <div class="board warning text center">
          <div class="reading">
            <p class="text large"><strong>Oops! Algo deu errado...</strong></p>
            <p>O conteúdo que você tentou acessar não está disponível.</p>
          </div>
        </div>
      <?php endif; ?>
    </div>
  </div>
</section>

<?php include 'section-customer-service.php'; ?>
