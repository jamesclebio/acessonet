<?php
class AuthenticationHelper
{
	public function sessionValidate() {
    $_SESSION['location'] = $_SERVER['REQUEST_URI'];

    if (!$_SESSION['profile']) {
      $_SESSION['profile'] = ProfileModel::getProfiles()[0];
    }

    if (!$_SESSION['area_id']) {
      header('Location: ' . $this->_url('session/set/area/1'));
      exit;
    }
  }

  public function getProfile() {
    return $_SESSION['profile'];
  }

  public function setProfile() {
    if (in_array($this->_get('profile'), ProfileModel::getProfiles())) {
      $_SESSION['profile'] = $this->_get('profile');
      header('Location: ' . $this->_url('root'));
      exit;
    } else {
      die('Invalid profile');
    }
  }

  public function getAreaId() {
    return $_SESSION['area_id'];
  }

  public function getAreaName() {
    return $_SESSION['area_name'];
  }

  public function getAreaFullName() {
    return $_SESSION['area_name'] . ', ' . $_SESSION['area_state'];
  }

  public function setArea() {
    foreach (AreaModel::getAreas() as $data) {
      if ($this->_get('area') == $data['id']) {
        $_SESSION['area_id'] = $data['id'];
        $_SESSION['area_name'] = $data['name'];
        $_SESSION['area_state'] = $data['state'];
        header('Location: ' . $_SESSION['location']);
        exit;
      }
    }
  }
}
