<?php
class Help extends Page
{
	public function __construct() {
		$this->setSession(true);
		$this->setAuthentication(true);
		$this->setLayout('default');
    $this->setView('help');
    $this->setTitle('Central de ajuda - Acessonet');
    $this->setDescription('A Acessonet é uma empresa de telecomunicações que oferece soluções inteligentes, integrando serviços de dados, voz e segurança, sempre preocupada com a garantia da qualidade suas entregas.');
    $this->setAnalytics(true);

    $this->helpPostsCategorie1 = HelpModel::getHelpPosts(1);
    $this->helpPostsCategorie2 = HelpModel::getHelpPosts(2);
    $this->helpPostsCategorie3 = HelpModel::getHelpPosts(3);
  }

  public function conteudo() {
    $this->setView('help-content');

    $this->helpPost = HelpModel::getHelpPost($this->_get('id'));
  }
}
