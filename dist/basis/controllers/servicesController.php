<?php
class Services extends Page
{
	public function __construct() {
    $this->setSession(true);
    $this->setAuthentication(true);
    $this->setLayout('default');
    $this->setView('services');
    $this->setTitle('Serviços - Acessonet');
    $this->setDescription('A Acessonet é uma empresa de telecomunicações que oferece soluções inteligentes, integrando serviços de dados, voz e segurança, sempre preocupada com a garantia da qualidade suas entregas.');
    $this->setAnalytics(true);
	}

  public function request() {
    $this->setLayout(false);
    $this->setView('services-request');

    $_SESSION['location'] = $this->_url('services');

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $this->form = false;
      $this->success = true;

      $this->firstName = strtok($_POST['name'], ' ');
    }
  }
}
