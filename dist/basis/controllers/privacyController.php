<?php
class Privacy extends Page
{
	public function __construct() {
		$this->setSession(true);
		$this->setAuthentication(true);
		$this->setLayout('default');
		$this->setView('privacy');
		$this->setTitle('Política de privacidade - Acessonet');
		$this->setDescription('A Acessonet é uma empresa de telecomunicações que oferece soluções inteligentes, integrando serviços de dados, voz e segurança, sempre preocupada com a garantia da qualidade suas entregas.');
		$this->setAnalytics(true);
	}
}
