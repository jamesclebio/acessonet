<?php
class About extends Page
{
	public function __construct() {
		$this->setSession(true);
		$this->setAuthentication(true);
		$this->setLayout('default');
		$this->setView('about');
		$this->setTitle('Sobre - Acessonet');
		$this->setDescription('A Acessonet é uma empresa de telecomunicações que oferece soluções inteligentes, integrando serviços de dados, voz e segurança, sempre preocupada com a garantia da qualidade suas entregas.');
		$this->setAnalytics(true);
	}
}
