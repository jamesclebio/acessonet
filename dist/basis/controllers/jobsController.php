<?php
class Jobs extends Page
{
	public function __construct() {
		$this->setSession(true);
		$this->setAuthentication(true);
		$this->setLayout('default');
		$this->setView('jobs');
		$this->setTitle('Trabalhe conosco - Acessonet');
		$this->setDescription('A Acessonet é uma empresa de telecomunicações que oferece soluções inteligentes, integrando serviços de dados, voz e segurança, sempre preocupada com a garantia da qualidade suas entregas.');
		$this->setAnalytics(true);
	}

  public function apply() {
    $this->setLayout(false);
    $this->setView('jobs-apply');

    $_SESSION['location'] = $this->_url('jobs');

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $this->form = false;
      $this->success = true;

      $this->firstName = strtok($_POST['name'], ' ');
    }
  }

	private function token($length = 20) {
		$token = null;
		$keys = range(0, 9);

		for ($i = 0; $i < $length; $i++)
			$token .= $keys[array_rand($keys)];

		return $token;
	}

	private function utf8($string, $from, $to) {
		$keys = array();
		$values = array();

		preg_match_all('/./u', $from, $keys);
		preg_match_all('/./u', $to, $values);

		$mapping = array_combine($keys[0], $values[0]);

		return strtr($string, $mapping);
	}

	private function plain($string) {
		$from = "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ";
		$to = "aaaaeeiooouucAAAAEEIOOOUUC";

		return $this->utf8($string, $from, $to);
	}

	private function uploadPrepare() {
		$path = 'download/curriculo/';
		$filename = strtolower($this->token() . '-' . $this->plain(preg_replace('/(.+?)\s.*/', '$1', $_POST['name'])) . '.' . end(explode('.', $_FILES['curriculum']['name'])));

		return $path . $filename;
	}

	public function form() {
		if ($_POST) {
			$upload_file = $this->uploadPrepare();

			if ($_FILES['curriculum']['type'] != 'application/pdf')
				$this->setAlert('<p><strong>Arquivo inválido!</strong></p><p>Certifique-se de que o arquivo selecionado é um PDF de até 2MB.</p>', 'error');
			else if (move_uploaded_file($_FILES['curriculum']['tmp_name'], $upload_file)) {
				$area = ($_POST['area'] == 'Outra') ? $_POST['area_other'] : $_POST['area'];

				$mail = new MailHelper();
				$mail->setHeader(
					'From: ' . $_POST['name'] . ' <' . $_POST['email'] . '>' . "\r\n" .
					'Reply-To: ' . $_POST['email'] . "\r\n"
				);
				$mail->setTo('contato@acessonet.com.br');
				$mail->setSubject('[Acessonet - Trabalhe conosco] - ' . $_POST['name']);
				$mail->setBody(
					'Nome: ' . 	$_POST['name'] . "\n" .
					'E-mail: ' . $_POST['email'] . "\n" .
					'Telefone: ' . $_POST['phone'] . "\n" .
					'Área que gostaria de atuar: ' . $area . "\n\n" .
					'---' . "\n" .
					'Currículo:' . "\n\n" .
					'http://' . $_SERVER['HTTP_HOST'] . $this->_url('root') . $upload_file . "\n\n" .
					'---' . "\n" .
					'Mais informações:' . "\n\n" .
					$_POST['info'] . "\n\n" .
					'*** Enviado a partir de ' . $_SERVER['HTTP_HOST'] . $this->_url('root') . ' em ' . date('d/m/Y H:i:s')
				);
				$mail->setAlertSuccess('<p><strong>Seus dados foram enviados com sucesso!</strong></p><p>Agradecemos pelo seu interesse em trabalhar na Acessonet.</p>');
				$mail->send($this);

				header('Location: ' . $this->_url('trabalhe'));
				exit;
			} else
				$this->setAlert('<p><strong>Ops! Algo deu errado... :(</strong></p><p>Por favor, tente novamente.</p><p>Se o erro persistir, fale com a gente através do <a href="' . $this->_url('atendimento') . '">nosso atendimento</a>.</p>', 'error');
		}
	}
}
