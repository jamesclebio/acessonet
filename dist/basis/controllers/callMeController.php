<?php
class CallMe extends Page
{
	public function __construct() {
    $this->setLayout(false);
    $this->setView('call-me');

    $this->form = true;
	}

  public function request() {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $this->form = false;
      $this->success = true;

      $this->firstName = strtok($_POST['name'], ' ');
    }
  }
}
