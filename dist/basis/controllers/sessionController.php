<?php
class Session extends Page
{
	public function __construct() {
    $this->setSession(true);
		$this->setLayout(null);
    $this->setView(null);
	}

  public function set() {
    switch (true) {
      case $this->_get('profile'):
        AuthenticationHelper::setProfile();
        break;

      case $this->_get('area'):
        AuthenticationHelper::setArea();
        break;
    }
  }
}
