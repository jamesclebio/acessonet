<!DOCTYPE html>
<html<?php $this->getHtmlAttribute(); ?> class="<?php $this->getHtmlClass(); ?>" lang="pt-BR">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $this->getTitle(); ?></title>
  <meta name="description" content="<?php echo $this->getDescription(); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo $this->_asset('styles/main.css'); ?>">
  <?php $this->getHeadAppend(); ?>
</head>
<body<?php $this->getBodyAttribute(); ?> class="<?php $this->getBodyClass(); ?>">
  <?php $this->getAnalytics(); ?>
  <?php $this->getBodyPrepend(); ?>

  <?php $this->getView(); ?>

  <script src="<?php echo $this->_asset('scripts/main.js'); ?>"></script>
  <?php $this->getBodyAppend(); ?>
</body>
</html>
