<?php
class PackageModel
{
  public function getPackages() {
    $database = Database::getInstance()->connect();

    $query = '
      SELECT
        *
      FROM
        packages
      WHERE
        area = ?
      AND
        status = 1
      ORDER BY
        price ASC
    ';

    $statement = $database->prepare($query);
    $statement->bind_param('i', $_SESSION['area_id']);
    $statement->execute();

    return $statement->get_result()->fetch_all(MYSQLI_ASSOC);
  }

  public function getPackage($id) {
    $database = Database::getInstance()->connect();

    $query = '
      SELECT
        *
      FROM
        packages
      WHERE
        id = ?
      AND
        area = ?
      AND
        status = 1
      ORDER BY
        price ASC
    ';

    $statement = $database->prepare($query);
    $statement->bind_param('ii', $id, $_SESSION['area_id']);
    $statement->execute();

    return $statement->get_result()->fetch_array(MYSQLI_ASSOC);
  }

  public function getPackagePriceMin() {
    $database = Database::getInstance()->connect();

    $query = '
      SELECT
        *
      FROM
        packages
      WHERE
        area = ?
      AND
        status = 1
      ORDER BY
        price ASC
      LIMIT
        1
    ';

    $statement = $database->prepare($query);
    $statement->bind_param('i', $_SESSION['area_id']);
    $statement->execute();

    return $statement->get_result()->fetch_array(MYSQLI_ASSOC);
  }

  public function getPackagePriceMax() {
    $database = Database::getInstance()->connect();

    $query = '
      SELECT
        *
      FROM
        packages
      WHERE
        area = ?
      AND
        status = 1
      ORDER BY
        price DESC
      LIMIT
        1
    ';

    $statement = $database->prepare($query);
    $statement->bind_param('i', $_SESSION['area_id']);
    $statement->execute();

    return $statement->get_result()->fetch_array(MYSQLI_ASSOC);
  }
}
