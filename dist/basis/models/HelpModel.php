<?php
class HelpModel
{
  public function getHelpPosts($categorie_id) {
    $database = Database::getInstance()->connect();

    $query = '
      SELECT
        help_posts.id,
        help_posts.title,
        help_posts.body,
        help_posts.created,
        help_posts.modified,
        help_posts.published,
        help_categories.name AS categorie
      FROM
        help_posts
      JOIN
        help_categories
      ON
        help_posts.categorie = help_categories.id
      WHERE
        help_posts.categorie = ?
      AND
        help_posts.status = 1
      ORDER BY
        help_posts.published DESC
      LIMIT
        30
    ';

    $statement = $database->prepare($query);
    $statement->bind_param('i', $categorie_id);
    $statement->execute();

    return $statement->get_result()->fetch_all(MYSQLI_ASSOC);
  }

  public function getHelpPost($post_id) {
    $database = Database::getInstance()->connect();

    $query = '
      SELECT
        help_posts.id,
        help_posts.title,
        help_posts.body,
        help_posts.created,
        help_posts.modified,
        help_posts.published,
        help_categories.name AS categorie
      FROM
        help_posts
      JOIN
        help_categories
      ON
        help_posts.categorie = help_categories.id
      WHERE
        help_posts.id = ?
      AND
        help_posts.status = 1
    ';

    $statement = $database->prepare($query);
    $statement->bind_param('i', $post_id);
    $statement->execute();

    return $statement->get_result()->fetch_array(MYSQLI_ASSOC);
  }
}
