<?php
/**
 * Basis
 *
 * @author James Clébio <jamesclebio@gmail.com>
 * @link https://github.com/jamesclebio/basis
 * @license http://opensource.org/licenses/MIT
 */

require_once 'setup.php';
require_once CORE . 'Autoload.php';

set_include_path(INCLUDES);

spl_autoload_register('Autoload::core');
spl_autoload_register('Autoload::helper');
spl_autoload_register('Autoload::model');
