;(($) => {
  'use strict';

  window.main = window.main || {};

  main.formToggle = {
    settings: {
      autoinit: true,
      main: '[data-form-toggle]'
    },

    init() {
      let $main = $(this.settings.main);

      if ($main.length) {
        this.handler($main);
      }
    },

    handler($main) {
      $main.on({
        submit: (event) => {
          this.action($(event.currentTarget));
          event.preventDefault();
        }
      });
    },

    action($form) {
      let $modal = $form.closest(main.modal.settings.main);
      let $container = $modal.length ? $modal.find('> .overlay > .container') : $form.parent();

      $.ajax({
        method: $form.attr('method') || 'POST',
        url: $form.attr('action'),
        data: $form.serialize(),

        beforeSend() {
          $form.find(':input').blur();

          return $modal.length ? main.modal.loading(true) : main.boxLoading.on($form.parent());
        },

        success(data) {
          if ($modal.length) {
            $container.html(data);
            setTimeout(() => main.modal.loading(false), 2000);
          } else {
            setTimeout(() => $container.html(data), 2000);
          }
        }
      });
    }
  };
})(jQuery);
