;(($) => {
  'use strict';

  window.main = window.main || {};

  main.formMask = {
    settings: {
      autoinit: true,
      main: '[data-mask]',
      options: {
        clearIncomplete: true
      },
      patterns: {
        date: '99/99/9999',
        phone: '(99) 9999[9]-9999',
        zipcode: '99999-999',
        cpf: '999.999.999-99',
        cnpj: '99.999.999/9999-99',
        currency: {
          prefix: 'R$ ',
          groupSeparator: '.',
          radixPoint: ',',
          placeholder: '0,00',
          numericInput: true,
          autoGroup: true
        }
      }
    },

    init() {
      let $main = $(this.settings.main);

      if ($main.length) {
        this.builder($main);
      }
    },

    builder($main) {
      for (let i = 0, length = $main.length; i < length; i++) {
        let $this = $($main[i]);
        let patternKey = $this.data('mask');
        let patternOptions = {};

        if (/\(autoblur\)/.test(patternKey)) {
          patternKey = patternKey.replace(/\(.+\)/, '');
          patternOptions.oncomplete = this.autoblur;
        }

        if (typeof this.settings.patterns[patternKey] === 'object') {
          Object.assign(patternOptions, this.settings.patterns[patternKey]);
          $this.inputmask(patternKey, patternOptions);
        } else {
          $this.inputmask(this.settings.patterns[patternKey], patternOptions);
        }
      }
    },

    autoblur() {
      let $inputs = $(this).closest('form').find(':input:not([disabled]):not([readonly]):not([name="redbutton"])');

      for (let i = 0, length = $inputs.length; i < length; i++) {
        if ($inputs[i] === this && i < length) {
          $inputs[i + 1].focus();

          return;
        }
      }
    }
  };
})(jQuery);
