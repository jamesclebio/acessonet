;(($) => {
  'use strict';

  window.main = window.main || {};

  main.mobileNav = {
    settings: {
      autoinit: true,
      main: '.default-header .nav-main',
      trigger: '.button-mobile',
      naviconOnClass: 'navicon-on',
      loginOnClass: 'login-on'
    },

    init() {
      this.handler();
    },

    handler() {
      let $main = $(this.settings.main);

      $main
        .find(this.settings.trigger)
        .on({
          click: (event) => {
            this.trigger($main, $(event.currentTarget));
            event.preventDefault();
        }
      });
    },

    trigger($main, $trigger) {
      switch (true) {
        case $trigger.hasClass('navicon'):
          $main.toggleClass(this.settings.naviconOnClass);
          break;

        case $trigger.hasClass('login'):
          $main.toggleClass(this.settings.loginOnClass);
          break;
      }
    }
  };
})(jQuery);
