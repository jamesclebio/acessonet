;(($) => {
  'use strict';

  window.main = window.main || {};

  main.pageLoading = {
    settings: {
      autoinit: false,
      main: '.page-loading',
      template: '<div class="page-loading"><div class="spinner"></div></div>',
      speed: 100
    },

    builder() {
      var $main = $(this.settings.main);

      return ($main.length) ? $main : $(this.settings.template).appendTo($('body'));
    },

    on() {
      this
        .builder()
        .fadeIn(this.settings.speed);
    },

    off() {
      this
        .builder()
        .fadeOut(this.settings.speed);
    }
  };
})(jQuery);
