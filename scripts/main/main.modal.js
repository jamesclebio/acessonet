;(($) => {
  'use strict';

  window.main = window.main || {};

  main.modal = {
    settings: {
      autoinit: true,
      main: '.modal',
      trigger: '[data-modal]',
      template: '<div class="modal"><div class="overlay"><button data-modal="close" autofocus></button><div class="spinner"></div><div class="container"></div></div></div>',
      loadingClass: 'loading',
      noScrollClass: 'noscroll',
      speed: 100,
      $body: $('body')
    },

    init() {
      this.handler();
    },

    builder($this) {
      let $main = $(this.settings.main);

      if ($main.length) {
        this.run($main, $this);
      } else {
        this.run($(this.settings.template).appendTo($('body')), $this);
      }
    },

    handler() {
      $(document).on({
        click: (event) => {
          this.builder($(event.currentTarget));
          event.preventDefault();
        }
      }, this.settings.trigger);

      $(document).on({
        keydown: (event) => {
          switch (event.which) {
            case 27:
              this.close();
              break;
          }
        }
      });
    },

    run($main, $this) {
      let target = ($this.data('modal')) ? $this.data('modal') : $this.attr('href');

      if (target === 'close') {
        this.close();
      } else {
        this.getContent($main, target);
      }
    },

    getContent($main, target) {
      let $container = $main.find('> .overlay > .container');

      $container.empty();
      this.settings.$body.addClass(this.settings.noScrollClass);

      if (/^#/.test(target)) {
        // Static content
        let $target = $(target);

        if ($target.length) {
          $container.html($target.clone());
        } else {
          this.error($container);
        }

        $main.fadeIn(this.settings.speed);
      } else {
        // Dynamic content
        $.ajax({
          context: this,
          url: target.replace(/^\((.+)\)$/, '$1'),

          beforeSend() {
            $main.fadeIn(this.settings.speed);
            this.loading(true);
          },

          error(jqXHR, textStatus, errorThrown) {
            this.error($container, `${textStatus}: ${errorThrown}`);
            this.loading(false);
          },

          success(data) {
            $container.html(data);
            this.loading(false);
          }
        });
      }
    },

    loading(on) {
      let $main = $(this.settings.main);

      if (on) {
        $main.addClass(this.settings.loadingClass);
      } else {
        $main.removeClass(this.settings.loadingClass);
      }
    },

    error($container, text) {
      if (text) {
        $container.text(text);
      } else {
        $container.text('Oops... :(');
      }
    },

    close() {
      this.settings.$body.removeClass(this.settings.noScrollClass);
      $(this.settings.main).fadeOut(this.settings.speed);
    }
  };
})(jQuery);
