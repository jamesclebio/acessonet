;(($) => {
  'use strict';

  window.main = window.main || {};

  main.roll = {
    settings: {
      autoinit: true,
      main: '[data-roll]',
      $htmlBody: $('html, body')
    },

    init() {
      this.handler();
    },

    handler() {
      $(document).on({
        click: (event) => {
          this.run($(event.currentTarget));
          event.preventDefault();
        }
      }, this.settings.main);
    },

    run($this) {
      let dataValue = this.getDataValue($this);
      let $target;

      if (/^#.+/.test(dataValue)) {
        $target = $(dataValue);
      }

      if (!$target) {
        $target = this.settings.$htmlBody;
      }

      if ($target.length) {
        this.settings.$htmlBody.animate({
          scrollTop: $target.offset().top
        }, 100);
      }
    },

    getDataValue($this) {
      if (/^#/.test($this.data('roll'))) {
        return $this.data('roll');
      } else {
        return $this.attr('href');
      }
    }
  };
})(jQuery);
