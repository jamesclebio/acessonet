;(($) => {
  'use strict';

  window.main = window.main || {};

  main.boxLoading = {
    settings: {
      autoinit: false,
      main: '.box-loading',
      template: '<div class="box-loading"><div class="spinner"></div></div>',
      speed: 100
    },

    builder($target) {
      var $main = $target.find(this.settings.main);

      if ($main.length) {
        return $main;
      } else if ($target.find('.control-bar:last-child').length) {
        return $(this.settings.template).prependTo($target);
      }

      return $(this.settings.template).appendTo($target);
    },

    on($target) {
      this
        .builder($target)
        .fadeIn(this.settings.speed);
    },

    off($target) {
      this
        .builder($target)
        .fadeOut(this.settings.speed);
    }
  };
})(jQuery);
