;(($) => {
  'use strict';

  window.main = window.main || {};

  main.tab = {
    settings: {
      autoinit: true,
      main: '[data-tab]',
      tabIndex: '.tab',
      onClass: 'on'
    },

    init() {
      this.handler();
      this.builder();
    },

    handler() {
      $(document).on({
        click: (event) => {
          this.run($(event.currentTarget));
          event.preventDefault();
        },

        change: (event) => {
          this.run($(event.currentTarget));
          event.preventDefault();
        }
      }, `${this.settings.main}:not(${this.settings.main.replace(']', '=content]')})`);
    },

    builder() {
      let $tabIndex = $(this.settings.tabIndex);

      if ($tabIndex.length) {
        for (let i = 0, length = $tabIndex.length; i < length; i++) {
          let $this = $($tabIndex[i]);
          let $triggers = $this.find(this.settings.main);
          let options = '';

          for (let i = 0, length = $triggers.length; i < length; i++) {
            let $this = $($triggers[i]);
            let dataValue = this.getDataValue($this);

            options += `<option value="${dataValue}">${$this.text()}</option>`;
          }

          $this.append(`<select data-tab>${options}</select>`);
        }
      }

      $(this.settings.main)
        .filter('.on')
        .trigger('click');
    },

    run($this) {
      let dataValue;

      if ($this[0].tagName === 'SELECT') {
        dataValue = $this.val();
      } else {
        dataValue = this.getDataValue($this);
      }

      let url = dataValue.replace(/^(#.+)?\((.+)\)/, '$2');
      let $target = $(dataValue.replace(/^(#.+)\(.+\)/, '$1'));
      let $content = $target.closest(this.settings.main);

      if (!url || url === dataValue) {
        this.showContent($target, $content);
      } else {
        this.getContent($target, $content, url);
      }
    },

    onClass($target) {
      let triggers = {
        $on: $(`[data-tab^="#${$target[0].id}"], [href="#${$target[0].id}"]`),
        $off: []
      };

      for (let i = 0, length = triggers.$on.length; i < length; i++) {
        let parents = $(triggers.$on[i]).parents();

        for (let i in parents) {
          let $items = $(parents[i]).find(this.settings.main);

          if ($items.length > 1) {
            triggers.$off.push($items.not(triggers.$on));
            break;
          }
        }
      }

      for (let i in triggers.$off) {
        triggers.$off[i].removeClass(this.settings.onClass);
      }

      triggers.$on.addClass(this.settings.onClass);

      // Select element
      let $options = $(`select${this.settings.main} option[value^="#${$target[0].id}"]`);

      for (let i = 0, length = $options.length; i < length; i++) {
        let $this = $($options[i]);

        $this.parent().val($this.attr('value'));
      }
    },

    getDataValue($this) {
      if (/^#/.test($this.data('tab'))) {
        return $this.data('tab');
      } else {
        return $this.attr('href') + $this.data('tab');
      }
    },

    getContent($target, $content, url) {
      $.ajax({
        context: this,
        url,

        beforeSend() {
          main.boxLoading.on($content);
        },

        error(jqXHR, textStatus, errorThrown) {
          let data = `${textStatus}: ${errorThrown}`;

          this.showContent($target, $content, data);
        },

        success(data) {
          this.showContent($target, $content, data);
        }
      });
    },

    showContent($target, $content, data) {
      if ($target.length) {
        $content
          .find('[id]')
          .hide();

        if (data) {
          $target.html(data);
        }

        $target.show();
        main.boxLoading.off($content);
        this.onClass($target);
      }
    }
  };
})(jQuery);
