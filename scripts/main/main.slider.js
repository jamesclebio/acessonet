;(($) => {
  'use strict';

  window.main = window.main || {};

  main.slider = {
    settings: {
      autoinit: true,
      main: '.slider',
      itemOnClass: 'on'
    },

    init() {
      var $main = $(this.settings.main);

      if ($main.length) {
        this.handler($main);
        this.builder($main);
      }
    },

    handler($main) {
      $main.on({
        init: (event, slick) => {
          $(slick.$slides[0]).addClass(this.settings.itemOnClass);
        },

        beforeChange: (event, slick, currentSlide) => {
          $(slick.$slides[currentSlide]).removeClass(this.settings.itemOnClass);
        },

        afterChange: (event, slick, currentSlide) => {
          $(slick.$slides[currentSlide]).addClass(this.settings.itemOnClass);
        }
      });
    },

    builder($main) {
      this.setHeight($main);
      this.particlesJS();

      $main.slick({
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 6000,
        arrows: false
      });
    },

    setHeight($main) {
      let heightMax = $(window).height() - $main.offset().top;
      let heightMin = 470;

      $main
        .find('.item')
        .add($main)
        .height((heightMax < heightMin ) ? heightMin : heightMax);
    },

    particlesJS() {
      let $particles = $('[id^="particles-js-"]');

      for (let i = 0, length = $particles.length; i < length; i++) {
        particlesJS.load($particles[i].id, 'assets/scripts/particles.json');
      }
    }
  };
})(jQuery);
