<?php if ($this->success): ?>
  <div class="board success text center">
    <div class="heading text mega">
      <i class="fa fa-check"></i>
    </div>

    <div class="reading">
      <p class="text large"><strong>Tudo certo, <?php echo $this->firstName; ?>!</strong></p>
      <p>Agora é só aguardar o contato de um de nossos especialistas, que será o mais breve possível.</p>
      <p><strong>Obrigado por escolher a Acessonet!</strong></p>
    </div>
  </div>
<?php endif; ?>
