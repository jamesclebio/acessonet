<section class="default-container">
  <ul class="breadcrumb">
    <li><a href="<?php echo $this->_url('root'); ?>">Home</a></li>
    <li class="on"><a href="<?php echo $this->_url('about'); ?>">A Acessonet</a></li>
  </ul>

  <div class="grid x-center">
    <div class="item size-8 text center">
      <h2 class="heading color highlighted-custom-2">Fique sempre conectado e mantenha a sua liberdade de escolha</h2>

      <div class="reading">
        <p>A Acessonet é uma empresa de telecomunicações que oferece soluções inteligentes, integrando serviços de dados, voz e segurança, sempre preocupada com a garantia da qualidade suas entregas.</p>
      </div>
    </div>
  </div>
</section>

<section class="background color-lightest">
  <div class="default-container">
    <div class="grid y-center">
      <div class="item">
        <h4 class="heading">Olhar diferenciado para o mercado</h4>

        <div class="reading">
          <p>A Acessonet oferece serviços que agregam valor a seus clientes, seja residencial ou empresa.</p>
        </div>
      </div>

      <div class="item text center">
        <div class="thumbnail size-400 transparent animation floating">
          <img src="<?php echo $this->_asset('images/cloud-storage.svg'); ?>" alt="">
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'section-packages.php'; ?>
