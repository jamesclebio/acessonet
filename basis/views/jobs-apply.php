<?php if ($this->success): ?>
  <div class="board success text center">
    <div class="heading text mega">
      <i class="fa fa-check"></i>
    </div>

    <div class="reading">
      <p class="text large"><strong>Currículo cadastrado!</strong></p>
      <p>O currículo de <strong><?php echo $this->firstName; ?></strong> foi cadastrado com sucesso.</p>
      <p>O processo seletivo é realizado pelo departamento de seleção da Acessonet.</p>
      <p><strong>Obrigado pelo interesse e boa sorte!</strong></p>
    </div>
  </div>
<?php endif; ?>
