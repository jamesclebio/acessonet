<section class="slider">
  <div class="item <?php echo $this->slide_random['theme']; ?> reverse" style="background-image: url('<?php echo $this->slide_random['background-image']; ?>');">
    <div id="particles-js-1"></div>
    <div class="default-container">
      <div class="block text">
        <div class="primary">A sua rotina pede uma internet que acompanhe o seu ritmo</div>
        <div class="secondary">A Acessonet facilita a sua vida com uma banda larga rápida e <strong>sem linha telefônica</strong></div>

        <?php if ($this->packagePriceMin): ?>
          <div class="price-tag">
            <div class="top">Planos a partir de</div>
            <div class="total"><small>R$</small><?php echo intval($this->packagePriceMin['price']); ?><small>,<?php echo explode('.', $this->packagePriceMin['price'])[1]; ?></small></div>
            <div class="plus">Com velocidade de até <strong><?php echo $this->packagePriceMax['download']; ?></strong></div>
          </div>
        <?php endif; ?>
      </div>

      <div class="block image">
        <img src="<?php echo $this->slide_random['image']; ?>" alt="">
      </div>
    </div>

    <a href="<?php echo $this->_url('planos'); ?>">Link</a>
  </div>

  <div class="item theme-3" style="background-image: url('<?php echo $this->_asset('images/background/fiber-gray.jpg'); ?>');">
    <div class="default-container">
      <div class="block text">
        <div class="primary">Quer Acessonet na sua casa?</div>
        <div class="secondary">Escolha o seu plano ideal e solicite assinatura</div>
        <a href="<?php echo $this->_url('planos'); ?>" class="button large warning">Conhecer planos</a>
      </div>

      <div class="block image">
        <img src="<?php echo $this->_asset('images/block/family.png'); ?>" alt="">
      </div>
    </div>
  </div>
</section>

<section class="default-container">
  <ul class="list-feature">
    <li>
      <img src="<?php echo $this->_asset('images/icon-feature/yellow/fast.png'); ?>" alt="">
      <div>Banda larga</div>
    </li>
    <li>
      <img src="<?php echo $this->_asset('images/icon-feature/yellow/fiber.png'); ?>" alt="">
      <div>Redes em fibra ótica</div>
    </li>
    <li>
      <img src="<?php echo $this->_asset('images/icon-feature/yellow/noline.png'); ?>" alt="">
      <div>Sem linha telefônica</div>
    </li>
    <li>
      <img src="<?php echo $this->_asset('images/icon-feature/yellow/nofranchise.png'); ?>" alt="">
      <div>Sem franquia</div>
    </li>
  </ul>
</section>

<section class="background color-lightest color custom-4 text center">
  <div class="default-container">
    <div class="grid y-center">
      <div class="item">
        <div class="thumbnail size-400 transparent animation floating">
          <img src="<?php echo $this->_asset('images/mackbook-happy.png'); ?>" alt="">
        </div>
      </div>

      <div class="item">
        <h4 class="heading">A banda larga é ideal para</h4>

        <ul class="list-iconbox">
          <li>
            <div class="icon">
              <i class="fa fa-facebook-official"></i>
            </div>
            Usar o Facebook
          </li>
          <li>
            <div class="icon">
              <i class="fa fa-film"></i>
            </div>
            Assistir ao Netflix
          </li>
          <li>
            <div class="icon">
              <i class="fa fa-youtube-play"></i>
            </div>
            Assistir ao YouTube
          </li>
          <li>
            <div class="icon">
              <i class="fa fa-whatsapp"></i>
            </div>
            Usar o WhatsApp
          </li>
          <li>
            <div class="icon">
              <i class="fa fa-music"></i>
            </div>
            Ouvir músicas
          </li>
          <li>
            <div class="icon">
              <i class="fa fa-gamepad"></i>
            </div>
            Jogar online
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>

<?php include 'section-packages.php'; ?>
