<section class="background color-lightest">
  <div class="default-container">
    <div class="grid x-center">
      <div class="item size-8 text center">
        <h2 class="heading color highlighted-custom-2">Alguma dúvida? Nós queremos lhe ajudar</h2>

        <div class="reading">
          <p>Aqui é o lugar ideal para esclarecer dúvidas e também para aprender coisas legais com algumas dicas e tutoriais que selecionamos para otimizar a sua experiência com os nossos serviços.</p>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="default-container">
  <ul class="breadcrumb">
    <li><a href="<?php echo $this->_url('root'); ?>">Home</a></li>
    <li class="on"><a href="<?php echo $this->_url('ajuda'); ?>">Central de ajuda</a></li>
  </ul>

  <div class="grid">
    <div class="item">
      <div class="text center">
        <div class="heading thumbnail size-100 transparent animation floating">
          <img src="<?php echo $this->_asset('images/icon-business/050-paper-plane.svg'); ?>" alt="">
        </div>

        <h4 class="heading">Comunicados</h4>
      </div>

      <?php if ($this->helpPostsCategorie1): ?>
        <ul class="list-link">
          <?php foreach ($this->helpPostsCategorie1 as $data): ?>
            <li><a href="<?php echo $this->_url('ajuda/conteudo/id/' . $data['id']); ?>" class="link warning"><small><?php echo date('d/m/Y', strtotime($data['published'])); ?></small> <?php echo $data['title']; ?></a></li>
          <?php endforeach; ?>
        </ul>

      <?php else: ?>
        <div class="text center color muted">
          <strong>Oops! Nada para mostrar aqui...</strong>
        </div>
      <?php endif; ?>
    </div>

    <div class="item">
      <div class="text center">
        <div class="heading thumbnail size-100 transparent animation floating">
          <img src="<?php echo $this->_asset('images/icon-business/096-headset.svg'); ?>" alt="">
        </div>

        <h4 class="heading">Dúvidas frequentes</h4>
      </div>

      <?php if ($this->helpPostsCategorie2): ?>
        <ul class="list-link">
          <?php foreach ($this->helpPostsCategorie2 as $data): ?>
            <li><a href="<?php echo $this->_url('ajuda/conteudo/id/' . $data['id']); ?>"><?php echo $data['title']; ?></a></li>
          <?php endforeach; ?>
        </ul>

      <?php else: ?>
        <div class="text center color muted">
          <strong>Oops! Nada para mostrar aqui...</strong>
        </div>
      <?php endif; ?>
    </div>

    <div class="item">
      <div class="text center">
        <div class="heading thumbnail size-100 transparent animation floating">
          <img src="<?php echo $this->_asset('images/icon-business/039-idea-1.svg'); ?>" alt="">
        </div>

        <h4 class="heading">Dicas e tutoriais</h4>
      </div>

      <?php if ($this->helpPostsCategorie3): ?>
        <ul class="list-link">
          <?php foreach ($this->helpPostsCategorie3 as $data): ?>
            <li><a href="<?php echo $this->_url('ajuda/conteudo/id/' . $data['id']); ?>"><?php echo $data['title']; ?></a></li>
          <?php endforeach; ?>
        </ul>

      <?php else: ?>
        <div class="text center color muted">
          <strong>Oops! Nada para mostrar aqui...</strong>
        </div>
      <?php endif; ?>
    </div>
  </div>
</section>

<?php include 'section-customer-service.php'; ?>
