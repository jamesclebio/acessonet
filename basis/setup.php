<?php
/**
 * Basis
 *
 * @author James Clébio <jamesclebio@gmail.com>
 * @link https://github.com/jamesclebio/basis
 * @license http://opensource.org/licenses/MIT
 */

define('ASSETS', 'assets/');
define('CACHE', 'basis/cache/');
define('CONTROLLERS', 'basis/controllers/');
define('CORE', 'basis/core/');
define('HELPERS', 'basis/helpers/');
define('INCLUDES', 'basis/views/includes/');
define('LAYOUTS', 'basis/layouts/');
define('MODELS', 'basis/models/');
define('TIMEZONE', 'America/Sao_Paulo');
define('VIEWS', 'basis/views/');
define('DB_HOST', '127.0.0.1');
define('DB_PORT', 3306);
define('DB_SOCKET', '');
define('DB_USER', 'root');
define('DB_PASSWORD', 'Pass1234');
define('DB_NAME', 'acessonet_site');

$controller_routes = array(
  'home' => 'index',
  'planos' => 'packages',
  'servicos' => 'services',
  'ajuda' => 'help',
  'atendimento' => 'customer-service',
  'sobre' => 'about',
  'privacidade' => 'privacy',
  'trabalhe' => 'jobs'
);
