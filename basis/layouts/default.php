<!DOCTYPE html>
<html<?php $this->getHtmlAttribute(); ?> class="<?php $this->getHtmlClass(); ?>" lang="pt-BR">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $this->getTitle(); ?></title>
  <meta name="description" content="<?php echo $this->getDescription(); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $this->_asset('images/favicon/apple-touch-icon.png'); ?>">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $this->_asset('images/favicon/favicon-32x32.png'); ?>">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $this->_asset('images/favicon/favicon-16x16.png'); ?>">
  <link rel="manifest" href="<?php echo $this->_asset('images/favicon/manifest.json'); ?>">
  <link rel="mask-icon" href="<?php echo $this->_asset('images/favicon/safari-pinned-tab.svg'); ?>" color="#5bbad5">
  <meta name="theme-color" content="#122F52">

  <link rel="stylesheet" href="<?php echo $this->_asset('styles/main.css'); ?>">

  <?php $this->getHeadAppend(); ?>
</head>
<body<?php $this->getBodyAttribute(); ?> id="top" class="<?php $this->getBodyClass(); ?>">
  <?php $this->getAnalytics(); ?>
  <?php $this->getBodyPrepend(); ?>

  <header class="default-header">
    <nav class="nav-session">
      <div class="default-container">
        <ul>
          <li <?php echo (AuthenticationHelper::getProfile() == 'residential') ? 'class="on"' : ''; ?>><a href="<?php echo $this->_url('session/set/profile/residential'); ?>">Você</a></li>
          <li <?php echo (AuthenticationHelper::getProfile() == 'business') ? 'class="on"' : ''; ?>><a href="<?php echo $this->_url('session/set/profile/business'); ?>">Empresa</a></li>
        </ul>

        <ul>
          <li class="phone"><a href="tel:07932117052">(79) 3211-7052</a></li>
          <li class="area"><a href="<?php echo $this->_url('area'); ?>" data-modal><?php echo AuthenticationHelper::getAreaFullName(); ?></a></li>
          <li class="packages"><a href="<?php echo $this->_url('planos'); ?>">Assine já</a></li>
        </ul>
      </div>
    </nav>

    <nav class="nav-main">
      <div class="default-container">
        <h1><a href="<?php echo $this->_url('root'); ?>">Acessonet</a></h1>

        <button class="button-mobile navicon"></button>
        <button class="button-mobile login"></button>

        <ul>
          <?php if (AuthenticationHelper::getProfile() == 'residential'): ?>
            <li><a href="<?php echo $this->_url('planos'); ?>">Planos</a></li>
          <?php endif; ?>
          <?php if (AuthenticationHelper::getProfile() == 'business'): ?>
            <li><a href="<?php echo $this->_url('servicos'); ?>">Serviços</a></li>
          <?php endif; ?>
          <li><a href="<?php echo $this->_url('ajuda'); ?>">Central de ajuda</a></li>
          <li><a href="<?php echo $this->_url('atendimento'); ?>">Atendimento</a></li>
        </ul>

        <form action="" method="post" target="_blank">
          <div class="field">
            <input type="email" name="email" placeholder="Email do assinante" required>
          </div>

          <div class="field">
            <input type="password" name="password" placeholder="Senha" required>
            <a href="#">Esqueceu?</a>
          </div>

          <button type="submit">Entrar</button>
        </form>
      </div>
    </nav>
  </header>

  <main class="default-main">
    <?php $this->getView(); ?>
  </main>

  <footer class="default-footer">
    <div class="support">
      <h2 class="heading color highlighted-custom-2 text thin"><a href="tel:07932117052">(79) 3211-7052</a></h2>

      <ul class="others">
        <li><a href="<?php echo $this->_url('atendimento'); ?>" class="link more">Mais atendimento</a></li>
        <li><a href="#" class="link more">Central do assinante</a></li>
      </ul>

      <div class="social">
        <h5 class="heading color highlighted-custom-2 text thin">Siga a Acessonet</h5>

        <ul>
          <li title="Instagram"><a href="#" target="_blank"><i class="fa fa-instagram"></i>acessonet</a></li>
          <li title="Twitter"><a href="#" target="_blank"><i class="fa fa-twitter"></i>acessonet</a></li>
          <li title="Facebook"><a href="#" target="_blank"><i class="fa fa-facebook-official"></i>acessonetprovedor</a></li>
        </ul>
      </div>
    </div>

    <div class="directions">
      <div class="default-container">
        <nav>
          <ul>
            <?php if (AuthenticationHelper::getProfile() == 'residential'): ?>
              <li><a href="<?php echo $this->_url('planos'); ?>">Planos</a></li>
            <?php endif; ?>
            <?php if (AuthenticationHelper::getProfile() == 'business'): ?>
              <li><a href="<?php echo $this->_url('servicos'); ?>">Serviços</a></li>
            <?php endif; ?>
            <li><a href="<?php echo $this->_url('ajuda'); ?>">Central de ajuda</a></li>
            <li><a href="<?php echo $this->_url('atendimento'); ?>">Atendimento</a></li>
            <li><a href="<?php echo $this->_url('sobre'); ?>"><small>A Acessonet</small></a></li>
            <li><a href="<?php echo $this->_url('privacidade'); ?>"><small>Política de privacidade</small></a></li>
            <li><a href="<?php echo $this->_url('trabalhe'); ?>"><small>Trabalhe conosco</small></a></li>
            <li><a href="#top" data-roll><small>Topo</small></a></li>
          </ul>
        </nav>

        <small class="copyright">Copyright &copy 2011-<?php echo date('Y'); ?> <strong>Acessonet</strong>. Todos os direitos reservados.</small>
      </div>
    </div>
  </footer>

  <script src="<?php echo $this->_asset('scripts/main.js'); ?>"></script>
  <?php $this->getBodyAppend(); ?>
</body>
</html>
