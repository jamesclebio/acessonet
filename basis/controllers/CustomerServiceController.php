<?php
class CustomerService extends Page
{
	public function __construct() {
		$this->setSession(true);
		$this->setAuthentication(true);
		$this->setLayout('default');
		$this->setView('customer-service');
		$this->setTitle('Atendimento - Acessonet');
		$this->setDescription('A Acessonet é uma empresa de telecomunicações que oferece soluções inteligentes, integrando serviços de dados, voz e segurança, sempre preocupada com a garantia da qualidade suas entregas.');
		$this->setAnalytics(true);

    $_SESSION['location'] = $this->_url('customer-service');
  }

  public function message() {
    $this->setLayout(false);
    $this->setView('customer-service-message');

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $this->form = false;
      $this->success = true;

      $this->firstName = strtok($_POST['name'], ' ');
    } else {
      $this->form = true;
      $this->success = false;
    }
  }

	public function form() {
		if ($_POST) {
			$mail = new MailHelper();
			$mail->setHeader(
				'From: ' . $_POST['name'] . ' <' . $_POST['email'] . '>' . "\r\n" .
				'Reply-To: ' . $_POST['email'] . "\r\n"
			);
			$mail->setTo('contato@acessonet.com.br');
			$mail->setSubject('[Acessonet - Contato site] - ' . $_POST['name']);
			$mail->setBody(
				'Nome: ' . $_POST['name'] . "\n" .
				'E-mail: ' . $_POST['email'] . "\n" .
				'Telefone: ' . $_POST['phone'] . "\n\n" .
				'---' . "\n" .
				'Mensagem:' . "\n\n" .
				$_POST['message'] . "\n\n" .
				'*** Enviado a partir de ' . $_SERVER['HTTP_HOST'] . ' em ' . date('d/m/Y H:i:s')
			);
			$mail->setAlertSuccess('<p><strong>Sua mensagem foi enviada com sucesso!</strong></p><p>Daremos um retorno o mais breve possível.</p>');
			$mail->send($this, $this->_url('atendimento'));
		}
	}
}
