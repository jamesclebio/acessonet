<?php
class Index extends Page
{
	public function __construct() {
    $this->setSession(true);
    $this->setAuthentication(true);
    $this->setLayout('default');
    $this->setView('index');
    $this->setTitle('Acessonet');
    $this->setDescription('A Acessonet é uma empresa de telecomunicações que oferece soluções inteligentes, integrando serviços de dados, voz e segurança, sempre preocupada com a garantia da qualidade suas entregas.');
    $this->setAnalytics(true);
    $this->getSlideRandom();
	}

  private function getSlideRandom() {
    $slides = array(
      array(
        'theme' => 'theme-2',
        'background-image' => $this->_asset('images/background/fiber-blue.jpg'),
        'image' => $this->_asset('images/block/woman.png')
      ),
      array(
        'theme' => 'theme-1',
        'background-image' => $this->_asset('images/background/fiber-yellow.jpg'),
        'image' => $this->_asset('images/block/man.png')
      ),
      array(
        'theme' => 'theme-1',
        'background-image' => $this->_asset('images/background/fiber-cyan.jpg'),
        'image' => $this->_asset('images/block/girl.png')
      )
    );

    $this->slide_random = $slides[array_rand($slides, 1)];
  }

  public function getProfileIndex() {
    $this->packagePriceMin = PackageModel::getPackagePriceMin();
    $this->packagePriceMax = PackageModel::getPackagePriceMax();

    switch (AuthenticationHelper::getProfile()) {
      case 'residential':
        include 'index-residential.php';
        break;

      case 'business':
        include 'index-business.php';
        break;
    }
  }
}
