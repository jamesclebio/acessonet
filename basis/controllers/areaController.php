<?php
class Area extends Page
{
	public function __construct() {
		$this->setLayout(null);
		$this->setView('area');
  }

  public function getAreas() {
    return AreaModel::getAreas();
  }
}
