<?php
class Packages extends Page
{
	public function __construct() {
    $this->setSession(true);
    $this->setAuthentication(true);
    $this->setLayout('default');
    $this->setView('packages');
    $this->setTitle('Planos - Acessonet');
    $this->setDescription('A Acessonet é uma empresa de telecomunicações que oferece soluções inteligentes, integrando serviços de dados, voz e segurança, sempre preocupada com a garantia da qualidade suas entregas.');
    $this->setAnalytics(true);
  }

  // public function st() {
  //   $_SESSION['profile'] = ProfileModel::getProfiles()[0];
  // }

  public function getPackages() {
    return PackageModel::getPackages();
  }

  public function request() {
    $this->setLayout(false);
    $this->setView('packages-request');

    $_SESSION['location'] = $this->_url('packages');

    $this->package = PackageModel::getPackage($this->_get('id'));
    $this->packageFullName = $this->package['name'] . ' ' . $this->package['tag'];

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $this->form = false;
      $this->success = true;

      $this->firstName = strtok($_POST['name'], ' ');
    } else {
      $this->form = true;
      $this->success = false;
    }
  }
}
