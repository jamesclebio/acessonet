<?php
class AreaModel
{
  public function getAreas() {
    $database = Database::getInstance()->connect();

    $query = '
      SELECT
        areas.id,
        areas.name,
        states.shortname AS state
      FROM
        areas
      JOIN
        states
      ON
        areas.state = states.id
      WHERE
        areas.status = 1
      ORDER BY
        areas.name ASC
    ';

    $result = $database->query($query) or die($database->error);

    return $result->fetch_all(MYSQLI_ASSOC);
  }
}
