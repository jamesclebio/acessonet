<?php
/**
 * Basis
 *
 * @author James Clébio <jamesclebio@gmail.com>
 * @link https://github.com/jamesclebio/basis
 * @license http://opensource.org/licenses/MIT
 */

class Database extends Singleton
{
  public function connect() {
    $connection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME, DB_PORT, DB_SOCKET)
      or die ('Could not connect to the database server' . mysqli_connect_error());

    return $connection;
  }
}
